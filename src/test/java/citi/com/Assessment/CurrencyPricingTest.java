package citi.com.Assessment;

import java.util.*;

import org.junit.Before;
import org.junit.Test;
import junit.framework.*;

public class CurrencyPricingTest {
	private CurrencyPricing price;
//	private String filename;
	@Before
	public void setup() throws Exception {
		price = new CurrencyPricing();
		
//		filename = "C:\\\\Users\\\\Administrator\\\\Downloads\\\\Assessment\\\\test.csv\"
	}
	@Test
	public void reading() {
		price.populateMap("test.csv");
	}
	@Test
	public void getFX1() {
		price.populateMap("test.csv");
		double val = price.getFXQuote("USD", "AED");
		Assert.assertEquals(val,3.6729);
	}
	@Test
	public void getFX2() {
		price.populateMap("test.csv");
		double val = price.getFXQuote("xxx", "AED");
		Assert.assertEquals(val,-1.0);
	}
	@Test
	public void getFX3() {
		price.populateMap("test.csv");
		double val = price.getFXQuote("AED", "ALL");
		Assert.assertEquals(val,27.728309);
	}
//	@Test
//	public void getFX4() {
////		price.populateMap("C:\\Users\\Administrator\\Downloads\\Assessment\\test.csv");
//		double val = price.getFXQuote("AED", "ALL");
//		Assert.assertEquals(val,0);
//	}
	

}
