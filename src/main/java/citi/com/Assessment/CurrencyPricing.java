package citi.com.Assessment;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.* ;

public class CurrencyPricing {
	private HashMap<String,Double> rateMap;
	
	public CurrencyPricing () {
		this.rateMap = new HashMap();		
	}

	
	public void populateMap(String filename) {
		String line ="";
		String split =",";
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(filename));
			while ((line = br.readLine()) != null) {
				
			                // use comma as separator
				String[] value = line.split(split);
				String key = value[0]+ "-" + value[1];
				double rate =  Double.parseDouble(value[2]);
				rateMap.put(key,rate);
//				System.out.println(country[0]+country[1]+country[2]);

			}
		}catch (FileNotFoundException e) {
			    e.printStackTrace();
		} catch (IOException e) {
			      e.printStackTrace();
		} finally {
			System.out.println(rateMap);
			  if (br != null) {
				  try {
			         br.close();
				  } catch (IOException e) {
					  e.printStackTrace();
			          }
			     }
			}
	}
	
	public double getFXQuote(String c1, String c2) {
		String key = c1+"-"+c2;
		
		if(rateMap.isEmpty()) {
			System.out.println("empty file");
			return 0.0;
		}
		if(rateMap.containsKey(key)) {
			return rateMap.get(key);
		}
		//return other stuff
		else if(rateMap.containsKey(c1+"-USD") && rateMap.containsKey("USD-"+c2)) {
			double val1 = rateMap.get(c1+"-USD");
			double val2 = rateMap.get("USD-"+c2);
			return val1*val2;	
		}
		return -1.0;
	}
}
			            
			

